# OpenVPN server autodeploy script
Author: Gennady Smirnov
ClubClever project
v. 1.0
easy to use.

# Usage:

1. Clone repository to new server:
`git clone https://dmlc@bitbucket.org/dmlc/open-vpn-autodeploy.git`
2. Change directory to repository folder:
`cd open-vpn-autodeploy`
3. Run script and provide answers to it questions.
`bash openvpn.sh`
4. Wait while it makes all operations.
5. Enjoy

# Important information:
Run this script only as root, in other case you can get wrong script work as result.
