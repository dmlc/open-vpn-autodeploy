#!/bin/bash
################################################################################
# Author:   Gennady Smirnov
#
# Web:      www.clubclever.spb.ru
#
# Program:
#   Purpose of this script is installing from aptitude OpenVPN server on clean
# Debian-like Linux server, configure it and make fully operational.
#
################################################################################
output() {
    printf "\E[0;33;40m"
    echo $1
    printf "\E[0m"
}

displayErr() {
    echo
    echo $1;
    echo
    exit 1;
}
clear
# Test if tun/tap is enabled
if test ! -e "/dev/net/tun"; then
        echo "TUN/TAP is not enabled. Please enable for this to work."
		exit
fi

output "Let's make this server OpenVPN again!"
output ""
output ""
output "Please enter the following required information to make setup DONE:"
output ""
#Ubuntu 16 server version requires non-bundled Open SSL to works fine (1.0.0g version builded from Sources)
   read -e -p "Enter server external IP address : " EXTIP
	 read -e -p "Enter internal VPN subnetwork e.g. 10.5.0.0 : " SUBN
	 read -e -p "Enter VPN server PORT for connections : " PORT
   read -e -p "Enter Connection type e.g. TCP or UDP : " CTYPE
   read -e -p "Do we need to connect new instance to RADIUS server? " RADFLAG
   read -e -p "Do we need to install BIND? " BINDFLAG
   if [[ ("$BINDFLAG" == "y" || "$BINDFLAG" == "Y") ]]; then
     read -e -p "Enter internal VPN IP from same subnetwork for BIND link : " BINDIP
   fi

   output "Enter data for SSL certificates:"
   output ""
   output "Please pay attention: Data shouldn't contain any spaces!!!"
   read -e -p "Please enter SSL Country : " country
	 read -e -p "Please enter SSL Country Province : " province
	 read -e -p "Please enter SSL City : " city
	 read -e -p "Please enter SSL Org. Name : " organization
	 read -e -p "Please enter SSL Email : " email
	 read -e -p "Please enter SSL Org. Unit : " organizationUnit
	 read -e -p "Please enter SSL KEY name : " commonName

	output "All Data accepted for processing"

clear

# update package and upgrade Debian
sudo apt-get -y update
sudo apt-get -y upgrade
sudo apt-get -y autoremove

output "Switching to Aptitude because apt-get didn't provide all required functions!"
output ""
sudo apt-get -y install aptitude
output "Installing required for OpenVPN server packages."
output ""
#Install MAKE
sudo aptitude -y install make
#Install libraries for Ubuntu 18 version
sudo aptitude -y install libgcrypt20 libgcrypt20-dev build-essential
#Install libraries for Ubuntu 16 version
sudo aptitude -y install libgcrypt11 libgcrypt11-dev build-essential
#Install libraries for Ubuntu UNIVERSAL
sudo aptitude -y install libgcrypt libgcrypt-dev build-essential
sudo aptitude -y install build-essential checkinstall zlib1g-dev libtemplate-perl

#Install OPEN VPN Server itself
sudo aptitude -y install openvpn easy-rsa
sudo service cron start
output "OpenVPN installed! Time to make certificates!"
sudo mkdir -p /etc/openvpn/easy-rsa/keys
sudo cp -rf /usr/share/easy-rsa/* /etc/openvpn/easy-rsa
sudo ln -s /etc/openvpn/easy-rsa/openssl-1.* /etc/openvpn/easy-rsa/openssl.cnf
sed -i "s/KEY_SIZE=.*/KEY_SIZE=4096/g" /etc/openvpn/easy-rsa/vars
sed -i 's/export CA_EXPIRE=3650/export CA_EXPIRE=365/' /etc/openvpn/easy-rsa/vars
sed -i 's/export KEY_EXPIRE=3650/export KEY_EXPIRE=365/' /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_COUNTRY=\"US\"/export KEY_COUNTRY=\"$country\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_PROVINCE=\"CA\"/export KEY_PROVINCE=\"$province\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_CITY=\"SanFrancisco\"/export KEY_CITY=\"$city\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_ORG=\"Fort-Funston\"/export KEY_ORG=\"$organization\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_EMAIL=\"me@myhost.mydomain\"/export KEY_EMAIL=\"$email\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_OU=\"MyOrganizationalUnit\"/export KEY_OU=\"$organizationUnit\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_NAME=\"EasyRSA\"/export KEY_NAME=\"$commonName\"/" /etc/openvpn/easy-rsa/vars
sed -i "s/export KEY_CN=openvpn.example.com/export KEY_CN=\"$commonName\"/" /etc/openvpn/easy-rsa/vars

#And starting SSL Proccessing
# Start generating keys and certificates
	cd /etc/openvpn/easy-rsa
	source ./vars > /dev/null 2>&1
	./clean-all  > /dev/null 2>&1
	./build-ca --batch  > /dev/null 2>&1
	./build-key-server --batch $commonName  > /dev/null 2>&1
	echo "Generating DH parameters, this will take a while(~45 minutes)."
	./build-dh > /dev/null 2>&1
	cd /etc/openvpn/easy-rsa/keys
	cp dh4096.pem ca.crt $commonName.crt $commonName.key /etc/openvpn
#BIND Installation if BIND flag active
  if [[ ("$BINDFLAG" == "y" || "$BINDFLAG" == "Y") ]]; then
sudo apt-get install -y bind9 bind9utils bind9-doc
echo 'OPTIONS="-u bind -4"' >> /etc/default/bind9
sudo systemctl restart bind9
  fi
#Open VPN server configuration process
cp /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz /etc/openvpn/
cd /etc/openvpn
gunzip -d /etc/openvpn/server.conf.gz
sed -i -e "s/port 1194/port $PORT/" /etc/openvpn/server.conf
sed -i -e "s/;proto tcp//" /etc/openvpn/server.conf
sed -i -e "s/proto udp/proto $CTYPE/" /etc/openvpn/server.conf
sed -i -e "s/;dev tap/ /" /etc/openvpn/server.conf
sed -i -e "s/ca/#ca/" /etc/openvpn/server.conf
sed -i -e "s/cert/#cert/" /etc/openvpn/server.conf
sed -i -e "s/key/#key/" /etc/openvpn/server.conf
sed -i -e "s/dh dh2048.pem/#dh dh2048.pem/" /etc/openvpn/server.conf
sed -i -e "s/ifconfig-pool-persist/;ifconfig-pool-persist/" /etc/openvpn/server.conf
sed -i -e "s/tls-auth/;tls-auth/" /etc/openvpn/server.conf
sed -i -e "s/cipher AES-256-CBC/;cipher AES-256-CBC /" /etc/openvpn/server.conf
sed -i -e "s/;comp-lzo/comp-lzo/" /etc/openvpn/server.conf
sed -i -e "s/explicit-exit-notify 1/;explicit-exit-notify 1/" /etc/openvpn/server.conf
sed -i -e "s/server 10.8.0.0 255.255.255.0/server $SUBN 255.255.255.0/" /etc/openvpn/server.conf
sed -i -e "s/ifconfig-pool-persist/;ifconfig-pool-persist/" /etc/openvpn/server.conf
echo "ca /etc/openvpn/ca.crt" >> /etc/openvpn/server.conf
echo "cert /etc/openvpn/server.crt" >> /etc/openvpn/server.conf
echo "key /etc/openvpn/server.key" >> /etc/openvpn/server.conf
echo "dh /etc/openvpn/dh2048.pem" >> /etc/openvpn/server.conf
echo "reneg-sec 0" >> /etc/openvpn/server.conf
echo "tun-mtu 1468" >> /etc/openvpn/server.conf
echo "tun-mtu-extra 32" >> /etc/openvpn/server.conf
echo "mssfix 1400" >> /etc/openvpn/server.conf
echo "push redirect-gateway def1" >> /etc/openvpn/server.conf
  if [[ ("$BINDFLAG" == "y" || "$BINDFLAG" == "Y") ]]; then
echo push "dhcp-option DNS $BINDIP" >> /etc/openvpn/server.conf
  fi
  if [[ ("$BINDFLAG" == "n" || "$BINDFLAG" == "N") ]]; then
echo 'push "dhcp-option DNS 8.8.8.8"' >> /etc/openvpn/server.conf
echo 'push "dhcp-option DNS 8.8.4.4"' >> /etc/openvpn/server.conf
  fi
sed -i -e "s/;log/log" /etc/openvpn/server.conf
sed -i -e "s/;log-append/log-append" /etc/openvpn/server.conf
echo "auth-user-pass-optional" >> /etc/openvpn/server.conf
sudo iptables -t nat -A POSTROUTING -s $SUBN/24 -o eth0 -j MASQUERADE
# Setup routing
  output "We are getting to setup routing right now"
	echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
  sysctl -p /etc/sysctl.conf > /dev/null 2>&1
  if [[ ("$RADFLAG" == "y" || "$RADFLAG" == "Y") ]]; then
  	 read -e -p "Please enter RADIUS server IP: " RADIP
  	 read -e -p "Please enter RADIUS secret for this server: " RADSECRET
     output "PLEASE PAY ATTENTION AND KEEP SECRET - YOU NEED TO ADD NAS ON YOUR RADIUS (DALORADIUS)"
     mkdir /etc/openvpn/radius
     wget https://www.nongnu.org/radiusplugin/radiusplugin_v2.1a_beta1.tar.gz -P /tmp
     mkdir /tmp/radiusplugin
     tar -zxvf /tmp/radiusplugin_v2.1a_beta1.tar.gz -C /tmp/radiusplugin
     make -C /tmp/radiusplugin/radiusplugin_v2.1a_beta1
     mv /tmp/radiusplugin/radiusplugin_v2.1a_beta1/radiusplugin.so /etc/openvpn/radius/
     mv /tmp/radiusplugin/radiusplugin_v2.1a_beta1/radiusplugin.cnf /etc/openvpn/radius/
     echo "plugin /etc/openvpn/radius/radiusplugin.so /etc/openvpn/radius/radiusplugin.cnf ifconfig-pool-persist ipp.txt persist-key" >> /etc/openvpn/server.conf
     sed -i -e "s/NAS-IP-Address=127.0.0.1/NAS-IP-Address=$EXTIP" /etc/openvpn/radius/radiusplugin.cnf
     sed -i -e "s/name=192.168.0.153/name=$RADIP" /etc/openvpn/radius/radiusplugin.cnf
     sed -i -e "s/sharedsecret=testpw/sharedsecret=$RADSECRET" /etc/openvpn/radius/radiusplugin.cnf
  fi
touch /var/log/openvpn/openvpn.log
touch /var/log/openvpn/openvpn-status.log
#Now we have to make client connection profile and we are done!
clear
output "We are done, but we need to create connection profile!"
output "Please stand by. after it done, you can download profile from script running folder"
output ""
output ""
output "Connection profile file name: vpn_profile.ovpn"

touch ./vpn_profile.ovpn
echo "client" >> ./vpn_profile.ovpn
echo "remote $EXTIP $PORT" >> ./vpn_profile.ovpn
echo "proto $CTYPE" >> ./vpn_profile.ovpn
echo "resolv-retry infinite" >> ./vpn_profile.ovpn
echo "route-delay 2" >> ./vpn_profile.ovpn
echo "pull" >> ./vpn_profile.ovpn
echo "comp-lzo yes" >> ./vpn_profile.ovpn
echo "dev tun" >> ./vpn_profile.ovpn
echo "nobind" >> ./vpn_profile.ovpn
echo "auth-user-pass" >> ./vpn_profile.ovpn
echo "<ca>" >> ./vpn_profile.ovpn
cat /etc/openvpn/ca.crt >> ./vpn_profile.ovpn
echo "</ca>" >> ./vpn_profile.ovpn
echo "<cert>" >> ./vpn_profile.ovpn
cat /etc/openvpn/server.crt >> ./vpn_profile.ovpn
echo "</cert>" >> ./vpn_profile.ovpn
echo "<key" >> ./vpn_profile.ovpn
cat /etc/openvpn/server.key >> ./vpn_profile.ovpn
echo "</key" >> ./vpn_profile.ovpn
#Restart daemon at the end.
sudo service openvpn restart
sudo ufw allow $PORT
sudo ufw allow 22
sudo ufw allow 10050
sudo ufw disable
sudo yes "y" | ufw enable
